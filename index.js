require('dotenv').config()
const express = require('express');
const axios = require('axios')
const expressSession = require('express-session');
const bodyParser = require("body-parser");
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const xmlParser = require('xml2js').parseString;

const db = require('./db');
const routes = require('./routes')
const  { User } = require('./models'); 

const app = express();
app.use(bodyParser.json({ limit: "50mb" }));
app.use(expressSession({
    secret: process.env.SESSION_SECRET,
    resave: false,
    saveUninitialized: false
}))

app.use(passport.initialize());
app.use(passport.session());


passport.use(new LocalStrategy(User.authenticate()));
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());


app.get('/', async (req, res) => {

    let feed = {};
    try {

        feed = await axios.get("http://joeroganexp.joerogan.libsynpro.com/rss")

        xmlParser(feed.data, {trim: true}, (err, result) => {

            result.rss.channel[0].item.map(podcast => {
                console.log(podcast.title[0], "\n")
            })
            res.send(result.rss.channel)
        })

    } catch(error) {
        console.log(error)
    }


})

routes(app)

app.listen(process.env.PORT, () => {
    console.log(`PodTalk is running on PORT:${process.env.PORT}`)
})